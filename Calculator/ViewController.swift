//
//  ViewController.swift
//  Calculator
//
//  Created by skale on 2/27/17.
//  Copyright © 2017 Strahinja. All rights reserved.
//

//Kontorler



import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var labela: UILabel!

    var kucanje = false
    
    
    @IBAction func touchDigit(_ sender: UIButton) {
        
        let digit = sender.currentTitle!
      
        if kucanje{
            
            let trenutniTekst = labela!.text!
            labela!.text = trenutniTekst + digit
            
        }else{
        
            labela!.text = digit
            kucanje = true
            
        }
        
    }
    
    
    var displejTekst : Double {
    
        get{
            
            return Double(labela.text!)!
            
        }
        set{
            
            labela.text = String(newValue)
        }
    
    
    }
    
    
    private var brain = CalculatorBrain()
    
    @IBAction func performOperation(_ sender: UIButton) {
        
        
        if kucanje {
        
            brain.setOperand(displejTekst)
            kucanje=false
            
        }
        
        if let matSimb = sender.currentTitle {
            
            brain.performOperation(matSimb)
            
            }
        
        if let result=brain.result{
        displejTekst=result
            
        }
        
    }

    
    
    
    
    
    
    
    
}

