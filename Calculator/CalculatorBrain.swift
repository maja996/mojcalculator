//
//  CalculatorBrain.swift
//  Calculator
//
//  Created by skale on 2/27/17.
//  Copyright © 2017 Strahinja. All rights reserved.
//


//Model 


import Foundation


struct CalculatorBrain {
    
    private var accumulator : Double? // not set
    
    
   mutating func performOperation(_ symbol : String){
        
        switch symbol {
        case "π" :
            
            accumulator = Double.pi
            
        case "√":
            if let operand = accumulator {  //ako je !null
                    accumulator = sqrt(operand)
            }
            
            
        default:
            break

        
        
        
    }
    }
    
    
    mutating func setOperand(_ operand: Double){
        accumulator = operand
    }
        
        
    
    var result: Double? {
        
        get {
            return accumulator
        }
    }
    
    
    
    
    
    
}
